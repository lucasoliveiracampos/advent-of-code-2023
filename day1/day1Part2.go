package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func wordToDigit(value string) string {
	m := map[string]string{
		"one":       "1",
		"two":       "2",
		"three":     "3",
		"four":      "4",
		"five":      "5",
		"six":       "6",
		"seven":     "7",
		"eight":     "8",
		"nine":      "9",
		"oneight":   "18",
		"twone":     "21",
		"threeight": "38",
		"fiveight":  "58",
		"nineeight": "98",
		"sevenine":  "79",
		"eightwo":   "82",
		"eighthree": "83",
		"1":         "1",
		"2":         "2",
		"3":         "3",
		"4":         "4",
		"5":         "5",
		"6":         "6",
		"7":         "7",
		"8":         "8",
		"9":         "9",
	}
	return m[value]
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	total := 0
	for scanner.Scan() {
		s := scanner.Text()
		r, _ := regexp.Compile("[1-9]|oneight|twone|threeight|fiveight|nineight|sevenine|eightwo|eighthree|one|two|three|four|five|six|seven|eight|nine")
		tuples := r.FindAllStringSubmatchIndex(s, -1)
		value1 := s[tuples[0][0]:tuples[0][1]]
		value2 := s[tuples[len(tuples)-1][0]:tuples[len(tuples)-1][1]]
		value := wordToDigit(value1) + wordToDigit(value2)
		if len(value) > 2 {
			intValue, _ := strconv.Atoi(string(rune(value[0])) + string(rune(value[len(value)-1])))
			total += intValue
		} else {
			intValue, _ := strconv.Atoi(value)
			total += intValue
		}
	}
	fmt.Println(total)
}
