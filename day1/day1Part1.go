package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	total := 0
	for scanner.Scan() {
		s := scanner.Text()
		r, _ := regexp.Compile("[0-9]")
		tuples := r.FindAllStringSubmatchIndex(s, -1)
		value1 := s[tuples[0][0]:tuples[0][1]]
		value2 := s[tuples[len(tuples)-1][0]:tuples[len(tuples)-1][1]]
		intValue, _ := strconv.Atoi(value1 + value2)
		total += intValue
	}
	fmt.Println(total)
}
