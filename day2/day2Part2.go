package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func part2(qtd int, color string) map[string]int {
	m := map[string]int{
		color: qtd,
	}
	return m
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	total := 0
	for scanner.Scan() {
		m := map[string]int{
			"red":   -1,
			"green": -1,
			"blue":  -1,
		}
		s := scanner.Text()
		r2 := regexp.MustCompile(`Game [0-9]+: `)
		s = r2.ReplaceAllString(s, "")
		subGames := strings.Split(s, ";")
		possible := true
		if possible {
			for _, game := range subGames {
				cubes := strings.Split(game, ",")
				for _, cube := range cubes {
					qtdCube := strings.Split(strings.Trim(cube, " "), " ")
					qtd, _ := strconv.Atoi(qtdCube[0])
					color := strings.Trim(qtdCube[1], " ")

					mLocal := part2(qtd, color)
					if mLocal[color] > m[color] {
						m[color] = mLocal[color]
					}
				}
			}
		}
		power := 1
		for _, value := range m {
			power *= value
		}
		total += power
		// fmt.Println(power)
	}
	fmt.Println(total)
}

//answer is to high: 3731
