# HOW TO RUN
1. Install [golang](https://go.dev/dl/)
1. Execute commands bellow:

```bash
go build
go run day1Part2.go < day1-part2-input.txt
```

Exercise's source reference: https://adventofcode.com/2023/day/2#part2