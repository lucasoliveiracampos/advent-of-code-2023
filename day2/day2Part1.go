package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func part1(qtd int, color string) bool {
	return !((color == "red" && qtd > 12) ||
		(color == "green" && qtd > 13) ||
		(color == "blue" && qtd > 14))

}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	r := regexp.MustCompile(`[0-9]+:`)
	total := 0
	for scanner.Scan() {
		s := scanner.Text()
		id := r.FindString(s)
		id = strings.Replace(id, ":", "", -1)
		idInt, _ := strconv.Atoi(id)
		// fmt.Println(id)
		r2 := regexp.MustCompile(`Game [0-9]+: `)
		s = r2.ReplaceAllString(s, "")
		subGames := strings.Split(s, ";")
		possible := true
		if possible {
			for _, game := range subGames {
				cubes := strings.Split(game, ",")
				for _, cube := range cubes {
					qtdCube := strings.Split(strings.Trim(cube, " "), " ")
					qtd, _ := strconv.Atoi(qtdCube[0])
					color := strings.Trim(qtdCube[1], " ")

					possible = part1(qtd, color)
					if !possible {
						break
					}
				}
				if !possible {
					break
				}
			}
			if possible {
				// fmt.Println(s)
				// fmt.Println(idInt)
				total += idInt
			}
		}
	}
	fmt.Println(total)
}

//answer is to high: 3731
