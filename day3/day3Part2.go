package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"unicode"
)

var picture []string = make([]string, 0)
var visited [][]bool = make([][]bool, 0)

func clear_visits() {
	for i := 0; i < len(visited); i++ {
		for j := 0; j < len(visited[i]); j++ {
			visited[i][j] = false
		}
	}
}

func get_number(line, column int) int {
	maxLeft, maxRight := column, column
	for maxLeft-1 >= 0 &&
		unicode.IsDigit(rune(picture[line][maxLeft-1])) &&
		!visited[line][maxLeft-1] {

		maxLeft -= 1
		visited[line][maxLeft] = true
	}
	for maxRight+1 < len(picture[line]) &&
		unicode.IsDigit(rune(picture[line][maxRight+1])) &&
		!visited[line][maxRight+1] {

		maxRight += 1
		visited[line][maxRight] = true
	}
	number, err := strconv.Atoi(picture[line][maxLeft : maxRight+1])
	if err != nil {
		fmt.Println("UNEXPECTED ERROR... PLEASE, FIX FIX ME =(")
		fmt.Println("line: ", line, " column: ", column, " maxLeft: ", maxLeft, " maxRight: ", maxRight)
		os.Exit(3)
	}
	return number
}

func look_left(line, column int) bool {
	return column-1 >= 0 &&
		!visited[line][column-1] &&
		unicode.IsDigit(rune(picture[line][column-1]))
}

func look_right(line, column int) bool {
	return column+1 < len(picture[line]) &&
		!visited[line][column+1] &&
		unicode.IsDigit(rune(picture[line][column+1]))
}

func look_down(line, column int) bool {
	return line+1 < len(picture) &&
		!visited[line+1][column] &&
		unicode.IsDigit(rune(picture[line+1][column]))
}

func look_down_left(line, column int) bool {
	return column-1 >= 0 && line+1 < len(picture) &&
		!visited[line+1][column-1] &&
		unicode.IsDigit(rune(picture[line+1][column-1]))
}

func look_down_right(line, column int) bool {
	// print(line, column)
	return line+1 < len(picture) && column+1 < len(picture[line+1]) &&
		!visited[line+1][column+1] &&
		unicode.IsDigit(rune(picture[line+1][column+1]))
}

func look_up(line, column int) bool {
	return line-1 >= 0 &&
		!visited[line-1][column] &&
		unicode.IsDigit(rune(picture[line-1][column]))
}

func look_up_left(line, column int) bool {
	return line-1 >= 0 && column-1 > 0 &&
		!visited[line-1][column-1] &&
		unicode.IsDigit(rune(picture[line-1][column-1]))
}

func look_up_right(line, column int) bool {
	return line-1 >= 0 && column+1 < len(picture[line-1]) &&
		!visited[line-1][column+1] &&
		unicode.IsDigit(rune(picture[line-1][column+1]))
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	r := regexp.MustCompile(`[^\.\d]`)
	total := 0
	for scanner.Scan() {
		s := scanner.Text()
		picture = append(picture, s)
		visited = append(visited, make([]bool, len(s)))
		// fmt.Println(s)
	}
	// fmt.Println(visited)
	for line_index, line := range picture {
		// fmt.Println(line)

		tuples := r.FindAllStringSubmatchIndex(line, -1)
		for _, tuple := range tuples {
			adjacentCount := make([]int, 0)
			current := line[tuple[0]:tuple[1]]
			// fmt.Println("current: ", current)
			var number int = 0
			clear_visits()
			if current == "*" {
				if look_left(line_index, tuple[0]) {
					// fmt.Println("look_left")
					adjacentCount = append(adjacentCount, get_number(line_index, tuple[0]-1))
				}
				if look_right(line_index, tuple[0]) {
					// fmt.Println("look_right")
					adjacentCount = append(adjacentCount, get_number(line_index, tuple[0]+1))
				}
				if look_down(line_index, tuple[0]) {
					// fmt.Println("look_down")
					adjacentCount = append(adjacentCount, get_number(line_index+1, tuple[0]))
				}
				if look_down_left(line_index, tuple[0]) {
					// fmt.Println("look_down_left")
					adjacentCount = append(adjacentCount, get_number(line_index+1, tuple[0]-1))
				}
				if look_down_right(line_index, tuple[0]) {
					// fmt.Println("look_down_right")
					adjacentCount = append(adjacentCount, get_number(line_index+1, tuple[0]+1))
				}
				if look_up(line_index, tuple[0]) {
					// fmt.Println("look_up")
					adjacentCount = append(adjacentCount, get_number(line_index-1, tuple[0]))
				}
				if look_up_left(line_index, tuple[0]) {
					// fmt.Println("look_up_left")
					adjacentCount = append(adjacentCount, get_number(line_index-1, tuple[0]-1))
				}
				if look_up_right(line_index, tuple[0]) {
					// fmt.Println("look_up_right")
					adjacentCount = append(adjacentCount, get_number(line_index-1, tuple[0]+1))
				}
			}
			if len(adjacentCount) == 2 {
				number = adjacentCount[0] * adjacentCount[1]
				// fmt.Println("adjacentCount[0], adjacentCount[1]", adjacentCount[0], adjacentCount[1])
			}
			// else if len(adjacentCount) > 2 {
			// 	fmt.Println("adjacentCount", adjacentCount)
			// }
			// fmt.Println("number line_index, tuple: ", number, line_index, tuple)
			total += number
		}
	}

	fmt.Println(total)
}

// attempt 1: wrong! answer 50400501 is too low
